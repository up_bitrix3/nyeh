﻿using atFrameWork2.SeleniumFramework;

namespace ATframework3demo.PageObjects
{
    public class TaskRedactionFrame
    {
        WebItem DeadlineField = new WebItem("//span [@data-bx-id='dateplanmanager-deadline']", "Поле выбора крайнего срока задачи");
        WebItem TaskSecondDeadline = new WebItem("//div[@class='bx-calendar-range bx-calendar-range-noline']//a[contains(@class, 'bx-calendar-date-hidden')][last()]", "Кнопка первичного дедлайна");
        WebItem BtnSaveChanges = new WebItem("//button[@class='ui-btn ui-btn-success']", "Кнопка Сохранить изменения");
        public TaskFrame ChangeDeadline()
        {
            DeadlineField.WaitElementDisplayed(5);
            DeadlineField.Click();
            TaskSecondDeadline.WaitElementDisplayed(5);
            TaskSecondDeadline.Click();
            BtnSaveChanges.Click();
            return new TaskFrame();

        }
    }
}