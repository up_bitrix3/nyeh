﻿using atFrameWork2.PageObjects;
using atFrameWork2.SeleniumFramework;

namespace ATframework3demo.PageObjects
{
    public class TaskFrame
    {
        WebItem BtnRedactionTask = new WebItem("//a[@class='task-view-button edit ui-btn ui-btn-link']", "Кнопка Редактирования Задачи");
        WebItem BtnTaskFrameClose = new WebItem("//div[@title='Закрыть']","Кнопка закрытия фрейма задачи");
        public TaskRedactionFrame OpenTaskRedaction()
        {
            BtnRedactionTask.WaitElementDisplayed(5);    
            BtnRedactionTask.Click();
            return new TaskRedactionFrame();
        }

        public TasksListPage CloseTask()
        {
            DriverActions.SwitchToDefaultContent();
            BtnTaskFrameClose.Click();
            DriverActions.Refresh();
            return new TasksListPage();
        }
    }
}
