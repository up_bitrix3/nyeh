﻿using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;

namespace ATframework3demo.PageObjects
{
    public class EventCreationPage
    {
        WebItem EventMonthlyRepetitionSelect= new WebItem("//div[@class='calendar-field-container']//option[@value='MONTHLY']", "Поле выбора повторяемости, каждый месяц");
        WebItem BtnCalendarEventCreationSave = new WebItem("//button[@class='ui-btn ui-btn-success']", "Кнопка 'Сохранить' событие");
        WebItem EventTitleInput = new WebItem("//input[@placeholder='Название события']", "Инпут названия события");
        WebItem EventStartingDateInputBox = new WebItem("//span[@class='calendar-field-container calendar-field-container-select-calendar'][1]", "Поле ввода даты начала события");
        WebItem EventDateOfStart = new WebItem("//a[contains(@class, 'bx-calendar-cell')and text()='18']", "Дата начала события");

        public EventCreationPage PickRepeatability()
        {
            EventMonthlyRepetitionSelect.Click();
            return new EventCreationPage();
        }
        public CalendarPage SaveNewEvent()
        {
            BtnCalendarEventCreationSave.Click();
            return new CalendarPage();
        }
        public EventCreationPage AddEventTitle(Bitrix24Task TestEvent)
        {
            EventTitleInput.Click();
            EventTitleInput.SendKeys(TestEvent.Title);
            return new EventCreationPage();
        }
        public EventCreationPage PickEventStartingDate()
        {
            EventStartingDateInputBox.Click();
            EventDateOfStart.WaitElementDisplayed();
            EventDateOfStart.Click();
            return new EventCreationPage();
        }
    
    
    }

}


