﻿using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;
using ATframework3demo.PageObjects;
using OpenQA.Selenium.DevTools.V108.Page;

namespace ATframework3demo.PageObjects
{

    public class CalendarPage
    {
        WebItem BtnCreateNewEvent = new WebItem("//button[@class='ui-btn-main']", "Кнопка создать событие");
        WebItem BtnSwitchCalendarMonth = new WebItem("//span [@class='calendar-navigation-next']", "Кнопка перейти на следующий месяц");
        WebItem EventCreationFrame = new WebItem("//div[@class='side-panel-content-container']", "Фрейм создания события");
        WebItem CreatedEvent(string eventName) => new WebItem($"//span[text()='{eventName}']", "Созданное событие на главной странице календаря");
        public EventCreationPage OpenEventCreationPage()
        {
            BtnCreateNewEvent.Click();
            EventCreationFrame.WaitElementDisplayed();
            return new EventCreationPage();

        }
        public string GetEventName(Bitrix24Task testEvent)
        {
            string EventName = CreatedEvent(testEvent.Title).GetAttribute("innertext");
            return EventName;
        }

        public void CheckCreatedEvents(Bitrix24Task testEvent)
        {
            int count = 0;

            while (count < 3)
            {

                if (CreatedEvent(testEvent.Title).AssertTextContains(testEvent.Title, "no events found"))
                {
                    count++;
                    BtnSwitchCalendarMonth.Click();
                }
                
                   
            }
           
            
                if (count == 3)
                {
                    Log.Info("В последующих 3 месяцах,включая текущий, успешно появилось событие");
                }
                else
                    Log.Error("В одном или нескольких из последующих месяцев не появилось созданное повторяющееся событие");
            

        
        }   
    }
}
