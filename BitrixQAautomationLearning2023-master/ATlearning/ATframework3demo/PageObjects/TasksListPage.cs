﻿using atFrameWork2.SeleniumFramework;
using ATframework3demo.PageObjects;
using atFrameWork2.TestCases;
using System.Threading.Tasks;
using atFrameWork2.TestEntities;
using OpenQA.Selenium.Internal;

namespace atFrameWork2.PageObjects
{
    public class TasksListPage
    {
       
        WebItem btnAddTask = new WebItem("//a[@id='tasks-buttonAdd']", "Кнопка создать задачу");

        WebItem TasksFrame = new WebItem("//iframe [@class='side-panel-iframe']", "Фрейм создания задачи");
        WebItem CreatedTaskName(string taskName) => new WebItem($"//a[@class='task-title task-status-text-color-in-progress' and text()='{taskName}']", "Созданная задача");
        WebItem TaskDeadLine(string taskName) => new WebItem($"//a[@class='task-title task-status-text-color-in-progress' and text()='{taskName}']//following::td[2]/descendant::span[@class='bxt-tasks-grid-deadline']", "Крайний Срок Задачи");


        public TaskCreationFrame TaskCreationFrameOpen()
        {
            btnAddTask.Click();
            TasksFrame.SwitchToFrame();
            return new TaskCreationFrame();
        }
        public TaskFrame ExistingTaskFrameOpen(Bitrix24Task testTask)
        {
            CreatedTaskName(testTask.Title).WaitElementDisplayed(5);
            CreatedTaskName(testTask.Title).Click();
            TasksFrame.SwitchToFrame();
            return new TaskFrame();
        }

        public string GetDeadLine(Bitrix24Task testTask)
        {
            string deadline = TaskDeadLine(testTask.Title).GetAttribute("innerText");
            return deadline; 
        }
    }
}