﻿using atFrameWork2.PageObjects;
using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;

namespace ATframework3demo.PageObjects
{
    public class TaskCreationFrame
    {

        WebItem TaskTitleInput = new WebItem("//input [@data-bx-id='task-edit-title']", "Инпут названия задачи");
        WebItem BtnSetTask = new WebItem("//button[@class='ui-btn ui-btn-success']", "Кнопка поставить задачу");
        WebItem DeadlineField = new WebItem("//span [@data-bx-id='dateplanmanager-deadline']", "Поле выбора крайнего срока задачи");
        WebItem TaskFirstDeadline = new WebItem("//div[@class='bx-calendar-range bx-calendar-range-noline']//a[contains(@class, 'bx-calendar-date-hidden')][last()]/preceding-sibling::a[1]", "Кнопка первичного дедлайна");



        public TasksListPage CreateNewTask(Bitrix24Task testTask)
        {
            TaskTitleInput.WaitElementDisplayed(5);
            TaskTitleInput.SendKeys(testTask.Title);
            DeadlineField.Click();
            TaskFirstDeadline.WaitElementDisplayed(5);
            TaskFirstDeadline.Click();
            BtnSetTask.Click();
            DriverActions.SwitchToDefaultContent();
            return new TasksListPage();
        }
       
    }


}
