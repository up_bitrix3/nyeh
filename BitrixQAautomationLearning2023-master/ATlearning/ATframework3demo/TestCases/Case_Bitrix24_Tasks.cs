﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;
using ATframework3demo.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace atFrameWork2.TestCases
{
    public class Case_Bitrix24_Tasks : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            return new List<TestCase>
            {
                new TestCase("Создание задачи", homePage => CreateTask(homePage)),
                new TestCase("Изменение Крайнего Срока задачи", homePage => TaskDeadlineChange(homePage))
            };
        }


        public void TaskDeadlineChange(PortalHomePage homePage)
        {   
            var task = new Bitrix24Task("testTasks" + DateTime.Now.Ticks);

            homePage
              .LeftMenu
              .OpenTasks() //открывает страницу задач
              .TaskCreationFrameOpen() //открывает фрейм создания задачи и переходит к нему
              .CreateNewTask(task); //создает заадачу с первым дедлайном и возвращает к списку задач
            string firstDeadline = new TasksListPage()
              .GetDeadLine(task); //достает дедлайн 1
            string secondDeadline = new TasksListPage()
              .ExistingTaskFrameOpen(task) //открывает созданную задачу
              .OpenTaskRedaction() //нажимает на кнопку Редактировать
              .ChangeDeadline() //меняет дедлайн на другое число
              .CloseTask() //возвращает на страницу со списком 
              .GetDeadLine(task); // достает дедлайн 2

            if (firstDeadline == secondDeadline)
            {
                Log.Error("Тест не пройден");
            }
            else
                Log.Info("Тест пройден");
        }

     
        public static void CreateTask(PortalHomePage homePage)
        {
            //код кейса здорового человека:
            homePage
                .LeftMenu
                .OpenTasks();

            //код кейса курильщика:
            var btnAddTask = new WebItem("//a[@id='tasks-buttonAdd']", "Кнопка добавления задачи");
            btnAddTask.Click();
            //свичнуться в слайдер
            var sliderFrame = new WebItem("//iframe[@class='side-panel-iframe']", "Фрейм слайдера");
            sliderFrame.SwitchToFrame();
            //ввести тайтл и описание
            var inputTaskTitle = new WebItem("//input[@data-bx-id='task-edit-title']", "Текстбокс названия задачи");
            var task = new TestEntities.Bitrix24Task("testTasks" + DateTime.Now.Ticks) { Description = "Какой то дескрипгш" + +DateTime.Now.Ticks };
            inputTaskTitle.SendKeys(task.Title);
            var editorFrame = new WebItem("//iframe[@class='bx-editor-iframe']", "Фрейм редактора текста");
            editorFrame.SwitchToFrame();
            var body = new WebItem("//body", "Это просто бади какой то");
            body.SendKeys(task.Description);
            DriverActions.SwitchToDefaultContent();
            sliderFrame.SwitchToFrame();
            //сохранить 
            var btnSaveTask = new WebItem("//button[@data-bx-id='task-edit-submit' and @class='ui-btn ui-btn-success']", "Кнопка сохранения задачи");
            btnSaveTask.Click();
            DriverActions.SwitchToDefaultContent();
            var gridTaskLink = new WebItem($"//a[contains(text(), '{task.Title}') and contains(@class, 'task-title')]", 
                $"Ссылка на задачу '{task.Title}' в гриде");
            gridTaskLink.WaitElementDisplayed();
            gridTaskLink.Click();
            sliderFrame.SwitchToFrame();
            //открыть задачу, ассертнуть тайтл и дескрипшн
            var taskTitleArea = new WebItem($"//div[@class='tasks-iframe-header']//span[@id='pagetitle']",
                "Область заголовка задачи"); 
            taskTitleArea.WaitElementDisplayed(10);
            taskTitleArea.AssertTextContains(task.Title, "Название задачи отображается неверно");
            var taskDescriptionArea = new WebItem($"//div[@id='task-detail-description']",
                "Область описания задачи");
            taskDescriptionArea.AssertTextContains(task.Description, "Название задачи отображается неверно");
        }
    }
}
