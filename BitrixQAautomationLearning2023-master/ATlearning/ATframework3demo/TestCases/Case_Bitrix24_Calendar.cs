﻿
using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;
using ATframework3demo.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ATframework3demo.TestCases
{
    public class Case_Bitrix24_Calendar : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            return new List<TestCase>
            {
                new TestCase("Cоздание повторяющегося события", homePage => RepeatingTaskCreation(homePage))
            };
        }



        public void RepeatingTaskCreation(PortalHomePage homePage)
        {

            var title = new Bitrix24Task("testEvent" + DateTime.Now.Ticks);
            homePage
                .LeftMenu
                .OpenCalendarPage()
                .OpenEventCreationPage()
                .AddEventTitle(title)
                .PickRepeatability()
                .PickEventStartingDate()
                .SaveNewEvent()
                .CheckCreatedEvents(title);



        }
    }
}
